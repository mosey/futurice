import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from 'js/App';
import { StoreProvider } from 'core/StoreContext';
import './src/scss/main.scss';

ReactDOM.render(
  <BrowserRouter>
    <StoreProvider>
      <App />
    </StoreProvider>
  </BrowserRouter>,
  document.getElementById('app')
);

// eslint-disable-next-line no-undef
if (process.env.NODE_ENV !== 'production') module.hot.accept();
