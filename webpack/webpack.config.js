/* eslint-disable no-undef */

const path = require('path');

const mainPath = path.join(__dirname, '../');
const srcPath = path.join(__dirname, '../src');

const loaders = require(path.join(__dirname, 'webpack.config.loaders'));

const ENV = process.env.NODE_ENV || 'development';
const envConfig = require(path.join(__dirname, 'webpack.config.' + ENV));

const mainConfig = {
  entry: {
    bundle: [
      `${mainPath}index.js`,
      `${srcPath}/scss/main.scss`
    ]
  },
  output: {
    path: path.join(__dirname, '..', 'dist', ENV),
    publicPath: '/',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
    alias: {
      js: `${srcPath}/js`,
      components: `${srcPath}/js/components`,
      core: `${srcPath}/js/core`,
      images: `${srcPath}/statics/images`,
      constants: `${srcPath}/js/constants`,
      reducers: `${srcPath}/js/reducers`,
      actions: `${srcPath}/js/actions`,
      helpers: `${srcPath}/js/helpers`,
      api: `${srcPath}/js/api`,
      static: `${srcPath}/js/components/static`,
      config: `${mainPath}/config`,
      scss: `${mainPath}/scss`
    }
  },
  module: loaders(ENV)
};

module.exports = Object.assign({}, mainConfig, envConfig);