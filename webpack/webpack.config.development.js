'use strict';
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const PrettierPlugin = require('prettier-webpack-plugin');

module.exports = {
  devtool: 'source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    // new BundleAnalyzerPlugin(),
    new PrettierPlugin({
      singleQuote: true,
      bracketSpacing: true
    }),
    new HtmlWebpackPlugin({
      inject: true,
      hash: true,
      favicon: './src/public/favicon-32x32.ico',
      template: './src/public/index.html',
      filename: 'index.html'
    })
  ],
  devServer: {
    contentBase: './dist',
    historyApiFallback: true,
    publicPath: '/',
    hot: true
  }
};
