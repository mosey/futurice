import styled from 'styled-components';
import { styling } from 'constants/styles/styles';

export const TopMenuWrapper = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 40px;
  height: 70px;
  background: white;
  position: fixed;
  z-index: 1;
  width: 100%;
  top: 0;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);

  & > div {
    padding: ${styling.spacer};
  }
`;

export const UserImage = styled.img`
  max-width: ${styling.spacerDouble};
  width: ${styling.spacerDouble};
  border-radius: 50%;
  margin-right: 20px;
`;

export const NavLinks = styled.nav`
  text-align: center;

  ul {
    display: flex;
    justify-content: space-around;
  }

  a {
    padding: 10px;
  }
`;
