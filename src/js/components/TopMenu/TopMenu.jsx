import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import md5 from 'md5';
import PropTypes from 'prop-types';
import { TopMenuWrapper, UserImage, NavLinks } from './TopMenuStyle';
import { withRouter } from 'react-router-dom';
import Login from 'js/components/Login/Login';
import { StoreContext } from 'core/StoreContext';
import { removeLoginDetails, logoutUser } from 'actions/auth';
import logo from 'images/main/imp-biz.jpg';
import { openModal } from 'actions/modal';

const TopMenu = () => {
  const {
    state: { auth },
    dispatch
  } = useContext(StoreContext);

  const { loggedIn = false, user: { name, email } = {} } = auth;

  const imgSrc = email
    ? `https://www.gravatar.com/avatar/${md5(email)}?s=50&d=identicon`
    : null;

  const logout = async () => {
    await removeLoginDetails();
    return dispatch(logoutUser());
  };

  return (
    <TopMenuWrapper>
      <Link to="/">
        <img src={logo} alt="IBI Inc." height="60" />
      </Link>
      <div className="u-flex-row">
        <NavLinks>
          <ul>
            {loggedIn && (
              <>
                <li>
                  <Link to="/">Images</Link>
                </li>
                <li>
                  <Link to="/upload">Upload</Link>
                </li>
                <li>
                  <a className="u-clickable" onClick={() => logout()}>
                    Logout
                  </a>
                </li>
              </>
            )}
          </ul>
        </NavLinks>
      </div>
      <div className="u-flex-row">
        {!loggedIn && (
          <div
            className="u-clickable"
            onClick={() => dispatch(openModal(<Login />))}
          >
            Login
          </div>
        )}
        {loggedIn && (
          <NavLinks>
            <ul>
              <li>
                <UserImage className="user" src={imgSrc} alt={email} />
                {name}
              </li>
            </ul>
          </NavLinks>
        )}
      </div>
    </TopMenuWrapper>
  );
};

TopMenu.propTypes = {
  value: PropTypes.string
};

TopMenu.defaultProps = {
  value: ''
};

export default withRouter(TopMenu);
