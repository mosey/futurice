export { default as Button } from 'static/Button/Button';
export { default as FormElement } from 'static/FormElement/FormElement';
export { default as RadioButton } from 'static/RadioButton/RadioButton';
export { default as Loading } from 'static/Loading/Loading';
export { default as Modal } from 'static/Modal/Modal';
