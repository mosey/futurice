import styled from 'styled-components';

export const Div = styled.div`
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  position: fixed;
  display: block;
  opacity: 1;
  background-color: #fff;
  z-index: 99;
  text-align: center;
`;

export const InnerDiv = styled.div`
  position: relative;
  top: 100px;
  left: 0;
  z-index: 100;
  text-align: center;
  margin: auto;
  opacity: 1;
`;
