import React from 'react';
import { Div, InnerDiv } from './LoadingStyle';

export default function Loading() {
  return (
    <Div>
      <InnerDiv>Loading....</InnerDiv>
    </Div>
  );
}
