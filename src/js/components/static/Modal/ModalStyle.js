import styled, { keyframes } from 'styled-components';

const slideUp = keyframes`
  from {
    opacity: 0;
    transform: translateY(50px);
  }

  to {
    opacity: 1;
    transform: translateY(0);
  }
`;

export const ModalMain = styled.div`
  position: fixed;
  width: 100vw;
  height: 100vh;
  z-index: 2;
  top: 0;
  left: 0;
  background: rgba(255, 255, 255, 0.7);
`;

export const ModalBg = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
`;

export const ModalWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ModalContent = styled.div`
  z-index: 3;
  display: flex;
  justify-content: center;
  min-width: 50vw;
  min-height: 50vh;
  max-height: 65vh;
  overflow-y: scroll;
  animation: ${slideUp} 0.3s forwards;
  position: relative;

  @media (max-width: 768px) {
    min-width: 95vw;
  }

  img {
    position: absolute;
    height: 100%;
  }
`;
