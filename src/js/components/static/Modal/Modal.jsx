import React, { useContext } from 'react';
import { StoreContext } from 'core/StoreContext';
import { ModalWrapper, ModalMain, ModalBg, ModalContent } from './ModalStyle';

export default function Modal() {
  const {
    state: { modal: { content } = {} },
    dispatch
  } = useContext(StoreContext);

  return (
    <ModalMain>
      <ModalBg onClick={() => dispatch({ type: 'CLOSE_MODAL' })} />

      <ModalWrapper>
        <ModalContent>{content}</ModalContent>
      </ModalWrapper>
    </ModalMain>
  );
}
