import React from 'react';
import PropTypes from 'prop-types';
import { Btn } from './ButtonStyle';

export default function Button({ children, ...props }) {
  const { onClick, disabled, type, size } = props;
  let color = type === 'secondary' ? 'secondaryTitle' : 'white';
  let colorBg = disabled ? `${type}Disabled` : type;
  return (
    <Btn
      onClick={onClick}
      color={color}
      colorBg={colorBg}
      size={size}
      disabled={disabled}
    >
      {children}
    </Btn>
  );
}

Button.propTypes = {
  onClick: PropTypes.func,
  type: PropTypes.string,
  size: PropTypes.string,
  color: PropTypes.string,
  disabled: PropTypes.bool
};

Button.defaultProps = {
  onClick: () => {},
  color: 'blue',
  size: '14',
  type: 'primary',
  disabled: false
};
