import styled from 'styled-components';
import { styling } from 'constants/styles/styles';

export const Btn = styled.button`
  font-family: 'Montserrat', sans-serif;
  background-color: ${props => `${styling[props.colorBg]}`};
  color: ${props => `${styling[props.color]}`};
  &:hover {
    background-color: ${props => `${styling[props.colorBg + 'Active']}`};
  }
  text-align: center;
  border-radius: 4px;
  padding: 10px 20px;
  border: 0;
  text-decoration: none;
  min-width: 120px;
`;
