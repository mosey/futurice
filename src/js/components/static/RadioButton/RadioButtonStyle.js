import styled from 'styled-components';
import { styling } from 'constants/styles/styles';

export const Input = styled.input`
  position: absolute;
  opacity: 0;
  &:checked + label {
    & > span {
      background-size: 9px;
      background-repeat: no-repeat;
      background-position: 50% 50%;
      background-color: ${styling['primaryGreen']};
      color: ${styling['primaryGreen']};
      min-width: 12px;
      width: 12px;
      min-height: 12px;
      height: 12px;
      top: 3px;
      left: 3px;
      border-radius: 50px;
      position: absolute;
    }
  }
`;

export const Label = styled.label`
  position: relative;
  padding-left: 25px;
  font-size: 14px;
  display: block;
  line-height: 1.29;
  color: ${styling['black']};
  &:hover {
    cursor: pointer;
  }
  &:after {
    content: '';
    height: 16px;
    width: 16px;
    color: #ffffff;
    line-height: 1.1;
    position: absolute;
    transition: background-color ease 0.3s;
    border: 1px solid #f0f0f0;
    left: 0;
    top: 0;
    margin-top: auto;
    margin-bottom: auto;
    font-size: 10px;
    text-align: center;
    border-radius: 50%;
  }
`;
