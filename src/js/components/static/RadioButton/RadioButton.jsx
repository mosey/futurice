import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Input, Label } from './RadioButtonStyle';

export default function RadioButton({ ...props }) {
  const { id, name, label, disabled, value, checked, onChange } = props;
  return (
    <Fragment>
      <Input
        type="radio"
        disabled={disabled}
        id={id}
        name={name}
        value={value}
        checked={checked}
        onChange={onChange}
      />
      <Label htmlFor={id}>
        {label}
        <span />
      </Label>
    </Fragment>
  );
}

RadioButton.propTypes = {
  value: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  id: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func
};

RadioButton.defaultProps = {
  value: '',
  id: 'id',
  label: 'Label',
  name: 'name',
  checked: false,
  disabled: false,
  onChange: () => {}
};
