import styled from 'styled-components';
import { styling } from 'constants/styles/styles';

export const MultiDiv = styled.div`
  position: absolute;
  overflow-y: auto;
  width: 100%;
  height: 100%;
  min-height: 0px;
  max-height: 300px;
  border: 1px solid #f4f8f8;
  padding: 10px;
`;

export const Div = styled.div`
  width: 100%;
  display: block;
  margin-bottom: ${styling.spacer};
  ${props => props.hidden && 'display: none'}
`;

export const InputHolder = styled.div`
  position: relative;
  ${props =>
    props.erroneous &&
    `border: 1px solid ${styling.red};
    `}
  &:after {
    ${props => props.multi && 'display: none'}

    content: '';
    display: block;
    height: 3px;
    bottom: 0;
    position: absolute;
    background: #4cb9f8;
    transition: all ease-in-out 0.75s;
    width: 0%;

    ${props =>
      props.selected &&
      `
      width: 100%;
      `}
  }
`;

export const Input = styled.input`
  border: none;
  background-color: ${styling.greyVeryLight};
  padding: 5px;
  font-size: 14px;
  width: 100%;
  padding: 15px 10px;
  border-radius: 4px;
  font-family: 'Montserrat', sans-serif;
  ${props => props.disabled && `color: ${styling.greyLight}`};
`;

export const FilterDiv = styled.div`
  position: sticky;
  top: -10px;
  z-index: 2;
  width: calc(100% + 20px);
  margin: -10px;
  background: white;
  padding: 10px;
`;

export const Textarea = styled.textarea`
  border: none;
  background-color: ${styling.greyVeryLight};
  padding: 5px;
  font-size: 14px;
  width: 100%;
  padding: 15px 10px;
  border-radius: 4px;
  font-family: 'Montserrat', sans-serif;
  ${props => props.disabled && `color: ${styling.greyLight}`}
`;

export const Select = styled.select`
  border: none;
  background-color: ${styling.greyVeryLight};
  padding: 5px;
  font-size: 14px;
  width: 100%;
  padding: 15px 10px;
  border-radius: 4px;
  font-family: 'Montserrat', sans-serif;
  height: 48px;
  ${props =>
    props.disabled &&
    `-webkit-appearance: none;
    color: ${styling.greyLight}`}
`;

export const Label = styled.label`
  text-transform: uppercase;
  display: block;
  margin-bottom: 10px;
  font-size: 10px;
  line-height: 1.3;
  letter-spacing: 1px;
  text-align: left;
  color: ${props =>
    props.disabled ? `${styling.grey}` : `${styling.primary}`};
`;

export const RedAsterixStyle = styled.span`
  color: ${styling.red};
`;

export const CheckBox = styled.input`
  position: absolute;
  opacity: 0;
  &:checked + label {
    &:after {
      background-image: url(${styling['imgCheckMark']});
      background-size: 9px;
      background-repeat: no-repeat;
      background-position: 50% 50%;
      background-color: ${styling['primaryGreen']};
      border: 1px solid ${styling['primaryGreen']};
      color: ${styling['primaryGreen']};
    }
  }
`;

export const CheckBoxLabel = styled.label`
  position: relative;
  padding-left: 25px;
  font-size: 14px;
  display: block;
  line-height: 1.29;
  color: ${styling['grey']};
  &:hover {
    cursor: pointer;
  }
  &:after {
    content: '';
    height: 13px;
    width: 13px;
    color: #ffffff;
    line-height: 1.1;
    position: absolute;
    transition: background-color ease 0.3s;
    border: 1px solid #f0f0f0;
    left: 0;
    top: 0;
    margin-top: auto;
    margin-bottom: auto;
    font-size: 10px;
    text-align: center;
    border-radius: 2px;
  }
`;

export const TooltipDiv = styled.div`
  position: absolute;
  top: 15px;
  right: 35px;
`;

export const ErrorDiv = styled.div`
  color: ${styling['red']};
  font-size: 10px;
  padding-top: 5px;
`;

export const MultiDivWrapper = styled.div`
  height: ${props => (props.editable ? '300px' : '100px')};
`;
