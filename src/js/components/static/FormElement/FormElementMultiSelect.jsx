import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Div,
  CheckBox,
  CheckBoxLabel,
  Input,
  MultiDiv,
  MultiDivWrapper,
  FilterDiv
} from './FormElementStyle';

export default function FormElementMultiSelect({
  values,
  onChange,
  name,
  data,
  label,
  editable
}) {
  const valuesToShow = editable
    ? values
    : values.filter(item => data[name] && data[name].includes(item.value));
  const [compValues, setValues] = useState(valuesToShow);
  if (!editable && !data[name]) return <div>Not Set</div>;
  const selected = data[name] || [];

  const filterData = val => {
    const newVals = compValues.filter(
      el => el.label.toLowerCase().indexOf(val.toLowerCase()) > -1
    );

    const set = val === '' ? values : newVals;
    return setValues(set);
  };

  return (
    <MultiDivWrapper editable={editable}>
      <MultiDiv>
        {editable && (
          <>
            <FilterDiv>
              <Input
                type="text"
                placeholder={`Search for ${label}`}
                onChange={e => filterData(e.target.value)}
              />
            </FilterDiv>
            <br />
          </>
        )}

        {compValues.map((el, i) => {
          return (
            <Div key={`${el.name}-${el.value}-${i}`}>
              <CheckBox
                onChange={e => onChange(e)}
                type="checkbox"
                id={el.value}
                name={name}
                value={el.value}
                checked={selected.indexOf(el.value) > -1}
              />
              <CheckBoxLabel htmlFor={el.value}>{el.label}</CheckBoxLabel>
            </Div>
          );
        })}
      </MultiDiv>
    </MultiDivWrapper>
  );
}

FormElementMultiSelect.propTypes = {
  values: PropTypes.array.isRequired,
  onChange: PropTypes.func,
  name: PropTypes.string,
  data: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  label: PropTypes.string,
  editable: PropTypes.bool
};

FormElementMultiSelect.defaultProps = {
  values: [],
  onChange: () => {},
  name: PropTypes.string,
  data: [],
  label: '',
  editable: true
};
