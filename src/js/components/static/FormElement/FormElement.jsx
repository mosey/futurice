import React, { useState } from 'react';
import PropTypes from 'prop-types';
import FormElementMultiSelect from './FormElementMultiSelect';

import {
  Input,
  Label,
  Div,
  InputHolder,
  RedAsterixStyle,
  Textarea,
  Select,
  CheckBox,
  CheckBoxLabel
} from './FormElementStyle';

export default function FormElement({ ...props }) {
  const [selected, toggleSelected] = useState(false);
  const {
    name,
    label,
    type,
    selectOptions,
    onChange,
    value,
    disabled,
    required,
    checked,
    noLabel,
    error,
    data,
    hidden,
    editable
  } = props;

  const onFocus = selected => {
    toggleSelected(selected);
  };

  const renderTextArea = (
    <Textarea
      name={name}
      type={type}
      onChange={e => onChange(e)}
      value={value}
      placeholder={label}
      required={required}
      disabled={disabled}
      cols="30"
      rows="5"
    />
  );

  const renderInput = (
    <Input
      onFocus={() => onFocus(true)}
      onBlur={() => onFocus(false)}
      type={type}
      name={name}
      placeholder={label}
      value={value}
      onChange={e => onChange(e)}
      disabled={disabled}
    />
  );

  const renderSelect = (
    <Select
      onFocus={() => onFocus(true)}
      onBlur={() => onFocus(false)}
      name={name}
      type={type}
      onChange={e => onChange(e)}
      value={value}
      disabled={disabled}
    >
      <option value={''}>Choose an option</option>
      {selectOptions &&
        selectOptions.map((item, idx) => {
          const value = typeof item !== 'object' ? item : item.value;
          const label = typeof item !== 'object' ? item : item.label;
          return (
            <option key={`${value}-${label}-${idx}`} value={value}>
              {label}
            </option>
          );
        })}
    </Select>
  );

  const renderCheckBox = (
    <>
      <CheckBox
        type="checkbox"
        disabled={disabled}
        checked={checked}
        id={name}
        name={name}
        onChange={e => onChange(e)}
      />
      <CheckBoxLabel htmlFor={name}>{label}</CheckBoxLabel>
    </>
  );

  const renderMulti = (
    <FormElementMultiSelect
      onChange={e => onChange(e)}
      values={selectOptions}
      name={name}
      data={data}
      label={label}
      editable={editable}
    />
  );

  const useType = () => {
    switch (type) {
      case 'textarea':
        return renderTextArea;
      case 'multiSelect':
        return renderMulti;
      case 'select':
        return renderSelect;
      case 'text':
      case 'number':
      case 'url':
      case 'password':
        return renderInput;
      case 'checkbox':
        return renderCheckBox;
    }
  };

  return (
    <Div hidden={hidden}>
      {!noLabel && (
        <Label disabled={disabled}>
          {label} {required ? <RedAsterixStyle>*</RedAsterixStyle> : ''}
        </Label>
      )}
      <InputHolder selected={selected} erroneous={error}>
        {useType()}{' '}
      </InputHolder>
    </Div>
  );
}

FormElement.propTypes = {
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  hidden: PropTypes.bool,
  required: PropTypes.bool,
  checked: PropTypes.bool,
  noLabel: PropTypes.bool,
  editable: PropTypes.bool,
  selectOptions: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(PropTypes.object)
  ]),
  onChange: PropTypes.func,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};

FormElement.defaultProps = {
  label: '',
  value: '',
  disabled: false,
  editable: false,
  hidden: false,
  required: false,
  checked: false,
  noLabel: false,
  selectOptions: [],
  isOpenable: false,
  onChange: () => {},
  error: false
};
