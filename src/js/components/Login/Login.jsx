import React, { useState, useContext, useEffect } from 'react';
import { LoginWrapper, LoginErrors } from './loginStyles';
import { setLoginDetails, loginUser } from 'actions/auth';
import { closeModal } from 'actions/modal';
import { FormElement, Button } from 'static';
import users from './users';

import { StoreContext } from 'core/StoreContext';

const Login = () => {
  const defaultUser = {
    email: '',
    password: ''
  };

  useEffect(() => {}, [errors]);

  const { dispatch } = useContext(StoreContext);
  const [user, setuser] = useState(defaultUser);
  const [errors, setErrors] = useState('');

  const handleLogin = async e => {
    e.preventDefault();
    const findUser = users.find(f => f.email === user.email);
    if (!findUser || (findUser && findUser.password !== user.password))
      return setErrors('Invalid email address or password, please try again');

    const deets = { email: findUser.email, name: findUser.name };

    await setLoginDetails(deets);
    dispatch(loginUser(deets));
    dispatch(closeModal());
  };

  const handleChange = (val, key) => {
    if (errors !== '') setErrors('');
    const obj = { [key]: val };
    setuser(Object.assign({}, user, obj));
  };

  return (
    <LoginWrapper>
      <h1>Login</h1>
      <br />
      {errors && <LoginErrors>{errors}</LoginErrors>}
      <br />
      <form onSubmit={e => handleLogin(e)}>
        <FormElement
          type="text"
          label="Email"
          value={user.email}
          name="login_email"
          onChange={e => handleChange(e.target.value, 'email')}
          placeholder="Enter your email address"
          error={errors !== ''}
          required
        />
        <FormElement
          type="password"
          label="Password"
          value={user.password}
          name="login_password"
          onChange={e => handleChange(e.target.value, 'password')}
          placeholder="Enter your password"
          error={errors !== ''}
          required
        />
        <Button>Login</Button>
      </form>
    </LoginWrapper>
  );
};

export default Login;
