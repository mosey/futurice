import styled from 'styled-components';

export const LoginWrapper = styled.div`
  background: white;
  width: 75%;
  height: 100%;
  margin: 0 auto;
  padding: 20px;
  box-shadow: 0px 3px 15px rgba(0, 0, 0, 0.2);
`;

export const LoginErrors = styled.p`
  color: red;
  font-size: 14px;
  margin-bottom: 10px;
`;
