export default [
  {
    id: 1,
    name: 'Ian Moss',
    password: 'password_ian',
    email: 'ianmoss101@gmail.com'
  },
  {
    id: 2,
    name: 'John Marston',
    password: 'cowboys',
    email: 'john@reddead.com'
  },
  {
    id: 3,
    name: 'Doom Guy',
    password: 'doomslayer',
    email: 'doom@slayer.com'
  },
  {
    id: 4,
    name: 'Konrad Titan',
    password: 'flashback',
    email: 'konrad@flashback.com'
  }
];
