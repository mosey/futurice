import styled from 'styled-components';

export const MainLayoutStyle = styled.div`
  width: 100%;
  max-width: 1200px;
  margin: auto;
  text-align: center;
  display: flex;
  min-height: 100%;
  padding: 90px 20px 0;
`;

export const ChildrenStyle = styled.div`
  text-align: center;
  width: 100%;
`;
