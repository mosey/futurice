import React, { Fragment, useContext } from 'react';
import { StoreContext } from 'core/StoreContext';
import TopMenu from 'js/components/TopMenu/TopMenu';
import { Modal } from 'static';

import { MainLayoutStyle, ChildrenStyle } from './MainLayoutStyle';

const MainLayout = ({ children }) => {
  const { state: { modal = {} } = {} } = useContext(StoreContext);

  return (
    <Fragment>
      {modal.open && <Modal />}
      <TopMenu />
      <MainLayoutStyle>
        <ChildrenStyle>{children}</ChildrenStyle>
      </MainLayoutStyle>
    </Fragment>
  );
};

export default MainLayout;
