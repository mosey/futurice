import axios from 'axios';

export async function uploadImage(data, config) {
  const resp = await axios.put('/images/add', data, config);
  return resp;
}

export async function fetchImages(params = {}) {
  const { data } = await axios.get('/images/', {
    params
  });

  return data.images;
}
