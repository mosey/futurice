export const IMG_EXTENSIONS = [
  'jpg',
  'jpeg',
  'gif',
  'png',
  'eps',
  'fla',
  'ico',
  'icon',
  'odg',
  'psd',
  'bmp',
  'tiff'
];
