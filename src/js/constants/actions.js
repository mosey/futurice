export const types = {
  LOG_IN: 'LOG_IN',
  LOG_OUT: 'LOG_OUT',
  SET_USER: 'SET_USER',
  OPEN_MODAL: 'OPEN_MODAL',
  CLOSE_MODAL: 'CLOSE_MODAL'
};
