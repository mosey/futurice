import React from 'react';
import MainLayout from 'js/components/Layouts/MainLayout/MainLayout';
import Images from '../components/Images';

const Home = () => {
  return (
    <MainLayout>
      <Images />
    </MainLayout>
  );
};

export default Home;
