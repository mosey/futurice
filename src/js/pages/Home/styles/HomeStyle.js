import styled, { keyframes } from 'styled-components';

const fadeIn = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

export const Image = styled.div`
  width: 200px;
  height: 200px;
  background-image: url(${props => props.src});
  background-size: cover;
  background-position: center;
  margin: 0 20px 20px 0;
  background-color: lightgrey;
  cursor: pointer;
  animation: ${fadeIn} forwards 0.4s;
  transform: scale(1, 1);
  transition: all ease-in-out 0.3s;
  overflow: hidden;

  @media (max-width: 960px) {
    width: 150px;
    height: 150px;
  }

  &:hover {
    transform: scale(1.1, 1.1);
  }
`;

export const ModalImage = styled.img`
  height: 100%;
`;

export const ImagesHeader = styled.div`
  margin: 20px 0;
  /* display: flex; */
  align-items: center;
  justify-content: space-around;

  > * {
    margin-bottom: 20px;
  }
`;

export const ImageList = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-wrap: wrap;
`;

export const ImageInfo = styled.div`
  text-align: center;
  padding: 20px;
  background: rgba(0, 0, 0, 0.7);
  color: white;
  font-size: 12px;
  height: 100%;
  opacity: 0;
  transform: translateY(100%);
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all ease 0.4s;

  ${Image}:hover & {
    opacity: 1;
    transform: translateY(0%);
  }

  li {
    margin-bottom: 20px;
  }
`;
