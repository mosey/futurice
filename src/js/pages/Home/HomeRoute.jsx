import React, { Fragment, lazy } from 'react';
import { Route } from 'react-router-dom';
import { WaitingComponent } from 'components/RouteComponents/RouteComponents/';
// import Home from 'js/pages/Home/routes/Home';
const Home = lazy(() => import('js/pages/Home/routes/Home'));

const HomeRoute = ({ match }) => {
  return (
    <Fragment>
      <Route path={match.path} render={WaitingComponent(Home)} />
    </Fragment>
  );
};

export default HomeRoute;
