import React, { useContext, useState, useEffect } from 'react';
import moment from 'moment';
import { StoreContext } from 'core/StoreContext';
import { Button } from 'static';
import {
  Image,
  ImagesHeader,
  ModalImage,
  ImageList,
  ImageInfo
} from '../styles/HomeStyle';
import { withRouter } from 'react-router-dom';
import { fetchImages } from 'api/images';
import { openModal } from 'actions/modal';

const Images = () => {
  const {
    state: {
      auth: {
        user: { email }
      }
    },
    dispatch
  } = useContext(StoreContext);

  const [images, setImages] = useState([]);
  const [errored, setErrored] = useState(false);
  const [viewingMine, setViewingMine] = useState(false);

  useEffect(() => {
    const getImages = async () => {
      const params = email && viewingMine ? { email } : null;
      try {
        const imgs = await fetchImages(params);
        setImages(imgs);
      } catch (err) {
        console.log('dicks');
        setErrored(true);
      }
    };
    getImages();
  }, [viewingMine, email]);

  if (images !== null && !Array.isArray(images) && images.length === 0)
    return <div>No images to display</div>;

  return (
    <div>
      <ImagesHeader>
        <h1>Here are all of our images</h1>
        {errored && <div>There has been an error fetching images</div>}
        {email && (
          <Button onClick={() => setViewingMine(!viewingMine)}>
            {viewingMine ? 'View all images' : 'View my images'}
          </Button>
        )}
      </ImagesHeader>
      <ImageList>
        {images !== null &&
          Array.isArray(images) &&
          images.map(el => (
            <Image
              onClick={() => dispatch(openModal(<ModalImage src={el.url} />))}
              src={el.url}
              key={el._id}
            >
              {email && (
                <ImageInfo>
                  <ul>
                    <li>{el.uploadedBy}</li>
                    <li>{el.email}</li>
                    <li>{moment(el.uploaded).fromNow()}</li>
                  </ul>
                </ImageInfo>
              )}
            </Image>
          ))}
      </ImageList>
    </div>
  );
};

export default withRouter(Images);
