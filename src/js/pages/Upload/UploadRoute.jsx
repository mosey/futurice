import React, { Fragment, lazy } from 'react';
import { Route } from 'react-router-dom';
import { WaitingComponent } from 'components/RouteComponents/RouteComponents';

const UploadRoute = ({ match }) => {
  const Upload = lazy(() => import('js/pages/Upload/routes/Upload'));

  return (
    <Fragment>
      <Route path={match.path} render={WaitingComponent(Upload)} />
    </Fragment>
  );
};

export default UploadRoute;
