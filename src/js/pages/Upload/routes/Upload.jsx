import React from 'react';
import MainLayout from 'js/components/Layouts/MainLayout/MainLayout';
import UploadImages from '../components/UploadImages';
const Home = () => {
  return (
    <MainLayout>
      <UploadImages />
    </MainLayout>
  );
};

export default Home;
