import React, { useState, useContext } from 'react';
import { StoreContext } from 'core/StoreContext';
import { withRouter } from 'react-router-dom';
import { uploadImage } from 'api/images';
import { Button } from 'static';
import {
  ProgIndicator,
  ProgBar,
  UploadPageWrapper,
  UploadForm
} from '../styles/UploadStyle';

const Images = () => {
  const {
    state: {
      auth: { user }
    }
  } = useContext(StoreContext);

  const [files, setFiles] = useState([]);
  const [progress, setProgress] = useState(0);
  const [complete, setComplete] = useState('');

  const handleFiles = e => {
    const addedFiles = [...e.target.files];

    if (addedFiles.length > 1)
      return alert('You may only upload one file at a time');
    return setFiles(addedFiles);
  };

  const uploadProgress = {
    onUploadProgress: progressEvent => {
      const percentCompleted = Math.round(
        (progressEvent.loaded * 100) / progressEvent.total
      );

      setProgress(percentCompleted);
    }
  };

  const handleSubmit = e => {
    e.preventDefault();
    setComplete('uploading');
    const formData = new FormData();

    files.forEach((file, i) => {
      formData.append(i, file);
    });
    formData.append('name', user.name);
    formData.append('email', user.email);

    uploadImage(formData, uploadProgress)
      .then(() => {
        setComplete('complete');
      })
      .catch(() => setComplete('error'));
  };

  const reset = () => {
    setFiles([]);
    setProgress(0);
    setComplete('');
  };

  return (
    <UploadPageWrapper>
      <h1>Upload your pictures</h1>
      <p>Use our handy form here to upload your awesome pictures!</p>

      <UploadForm onSubmit={e => handleSubmit(e)}>
        <div>
          {complete === 'complete' && (
            <div className="u-clickable">
              <h3>Image uploaded successfully</h3>
              <p onClick={() => reset()}>Click here to upload another one.</p>
            </div>
          )}

          {complete === 'uploading' && <h3>Uploading</h3>}
          {complete === 'error' && (
            <h3>There has been an error uploading your image</h3>
          )}
          {complete === '' && (
            <>
              <input onChange={e => handleFiles(e)} type="file" />
              <Button>Submit</Button>
            </>
          )}
        </div>

        <ProgBar>
          <span>
            {files.length > 0 ? files[0].name : 'Select a funky file to upload'}
          </span>
          <ProgIndicator width={`${progress}%`} />
        </ProgBar>
        {progress === 100 && complete === '' && <h3>Processing...</h3>}
      </UploadForm>
    </UploadPageWrapper>
  );
};

export default withRouter(Images);
