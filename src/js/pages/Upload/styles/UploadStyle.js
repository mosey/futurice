import styled from 'styled-components';

export const UploadPageWrapper = styled.div`
  width: 100%;
  margin-top: 20px;
  > * {
    margin-bottom: 20px;
  }
`;

export const UploadForm = styled.form`
  > * {
    margin-bottom: 20px;
  }
`;

export const ProgBar = styled.div`
  margin: 20px 0;
  width: 50%;
  height: 35px;
  border: 1px solid rgba(0, 0, 0, 0.1);
  position: relative;
  font-style: italic;
  font-size: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

export const ProgIndicator = styled.div`
  width: ${props => props.width};
  height: 100%;
  position: absolute;
  transition: all ease 0.4s;
  left: 0;
  top: 0;
  background: orange;
`;
