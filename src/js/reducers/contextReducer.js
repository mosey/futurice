import { types } from 'constants/actions';
import { user } from 'constants/user';

const contextReducer = (state = {}, action) => {
  switch (action.type) {
    case types.SET_USER:
      return {
        ...state,
        user: action.user
      };

    case types.LOG_IN:
      return {
        ...state,
        auth: { loggedIn: true, user: action.payload }
      };

    case types.LOG_OUT:
      return {
        ...state,
        auth: { loggedIn: false, user }
      };

    case types.OPEN_MODAL:
      return {
        ...state,
        modal: {
          open: true,
          content: action.payload
        }
      };

    case types.CLOSE_MODAL:
      return {
        ...state,
        modal: {}
      };

    default:
      throw new Error('Unexpected action');
  }
};
export { types, contextReducer };
