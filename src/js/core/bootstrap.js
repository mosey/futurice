import { checkIfAuthorised } from 'actions/auth';
import networkService from 'core/networkService';

// Bootstrap the app before initial render so we can set env variables, configs, check if we're logged in etc
const bootstrap = async () => {
  const user = await checkIfAuthorised();
  networkService.setDefaults();
  return user;
};

export default bootstrap;
