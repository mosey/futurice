import React, { lazy, useContext } from 'react';
import { Switch, Route } from 'react-router-dom';
import {
  PrivateRoute,
  WaitingComponent
} from 'components/RouteComponents/RouteComponents';
import { StoreContext } from './StoreContext';

const HomeRoute = lazy(() => import('js/pages/Home/HomeRoute'));
// import HomeRoute from 'js/pages/Home/HomeRoute';

const UploadRoute = lazy(() => import('js/pages/Upload/UploadRoute'));

const Routes = () => {
  const {
    state: { auth }
  } = useContext(StoreContext);
  return (
    <main className="l-site-wrapper">
      <Switch>
        <Route exact path="/" render={WaitingComponent(HomeRoute)} />
        <PrivateRoute
          isAuthenticated={auth.loggedIn}
          path="/upload"
          component={WaitingComponent(UploadRoute)}
        />
      </Switch>
    </main>
  );
};

export default Routes;
