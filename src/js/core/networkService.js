import axios from 'axios';
import config from 'config';

export default {
  setDefaults: () => {
    axios.defaults.baseURL = config.baseUrl;
  }
};
