import React, { useReducer, createContext } from 'react';
import { contextReducer } from 'reducers/contextReducer';

const initialState = {
  auth: { loggedIn: false, token: '' }
};

const StoreContext = createContext(initialState);
const StoreProvider = ({ children }) => {
  const initialState = {
    auth: {
      loggedIn: false,
      user: { email: '', name: '' }
    },
    modal: { open: false, content: {} }
  };
  // Get state and dispatch from Reacts new API useReducer.
  const [state, dispatch] = useReducer(contextReducer, initialState);

  return (
    <StoreContext.Provider value={{ state, dispatch }}>
      {children}
    </StoreContext.Provider>
  );
};
export { StoreContext, StoreProvider };
