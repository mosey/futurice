import localforage from 'localforage';

// // Check if authorised by checking localstorage values
export const checkIfAuthorised = async () => {
  const userDetails = await localforage.getItem('userDetails');
  if (!userDetails) return null;
  return userDetails;
};

export const setLoginDetails = async details => {
  await localforage.setItem('userDetails', details);
};

export const removeLoginDetails = async () => {
  await localforage.removeItem('userDetails');
};

export const loginUser = details => ({
  type: 'LOG_IN',
  payload: details
});

export const logoutUser = () => ({
  type: 'LOG_OUT'
});
