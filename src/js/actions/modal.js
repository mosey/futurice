export const openModal = (payload = {}) => {
  return {
    type: 'OPEN_MODAL',
    payload
  };
};

export const closeModal = () => ({ type: 'CLOSE_MODAL' });
