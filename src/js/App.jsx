import React, { useEffect, useState, useContext } from 'react';
import { StoreContext } from 'core/StoreContext';
import Routes from 'core/Routes';
import bootstrap from 'core/bootstrap';
import { loginUser } from 'actions/auth';

const App = () => {
  const [loaded, setLoaded] = useState(false);
  const { state, dispatch } = useContext(StoreContext);

  useEffect(() => {
    const userDetails = async () => {
      const userDetails = await bootstrap();
      userDetails && dispatch(loginUser(userDetails));
      setLoaded(true);
    };
    userDetails();
  }, [state.auth.loggedIn]);

  if (!loaded) return <div>Loading</div>;

  return <Routes />;
};

export default App;
